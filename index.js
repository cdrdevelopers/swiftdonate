var request = require("request");
var moment = require("moment");
var https = require("https");
var amazonPayments = require("amazon-payments");
var StringBuilder = require("stringbuilder");
//var querystring = require("querystring");
var URI = require("urijs");
var HashMap = require("hashmap");
//var TreeMap = require("treemap-js");
var dateFormat = require("dateformat");
var CryptoJS = require("crypto-js");
var crypto = require("crypto");
var mws = require("mws-simple") ({
    accessKeyId:'AKIAIQ2ERYWRXMMUDFJA',
    secretAccessKey:'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',
    merchantId: 'AV0ROZ839GLVJ'
});

var CHARACTER_ENCODING = "UTF-8";
//var PROD_ACCESS_KEY_ID = 'AKIAIQ2ERYWRXMMUDFJA';
//var PROD_SECRET_KEY = '2jyHYLVIhcoWwRqhQtHe7yaQ+6kd4NzlwNL+FTfM';
var CLIENT_ID = 'amzn1.application-oa2-client.5bb4bac9492d4388b522139ca0d3da6e';

/**
 * This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
 * The Intent Schema, Custom Slots, and Sample Utterances for this skill, as well as
 * testing instructions are located at http://amzn.to/1LzFrj6
 *
 * For additional samples, visit the Alexa Skills Kit Getting Started guide at
 * http://amzn.to/1LGWsLG
 */

// Route the incoming request based on type (LaunchRequest, IntentRequest,
// etc.) The JSON body of the request is provided in the event parameter.

// --------------- AWS handler export -----------------------

exports.handler = function (event, context) {
    try {
        console.log("event.session.application.applicationId=" + event.session.application.applicationId);

        /*if (event.session.application.applicationId !== "amzn1.echo-sdk-ams.app.bb9a6294-631b-4f0e-8a00-a423a0db0f72") {
             context.fail("Invalid Application ID");
        }*/

        if (event.session.new) {
            onSessionStarted({requestId: event.request.requestId}, event.session);
        }

        if (event.request.type === "LaunchRequest") {
            onLaunch(event.request, event.session, function callback(sessionAttributes, speechletResponse) {
                context.succeed(buildResponse(sessionAttributes, speechletResponse));
            });
        } else if (event.request.type === "IntentRequest") {
            onIntent(event.request, event.session, function callback(sessionAttributes, speechletResponse) {
                context.succeed(buildResponse(sessionAttributes, speechletResponse));
            });
        } else if (event.request.type === "SessionEndedRequest") {
            onSessionEnded(event.request, event.session);
            context.succeed();
        }

    } catch (e) {
        context.fail("Exception: " + e);
    }
};

// --------------- Alexa functions -----------------------

/**
 * Called when the session starts.
 */
function onSessionStarted(sessionStartedRequest, session) {
    console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId + ", sessionId=" + session.sessionId);
}

/**
 * Called when the user launches the skill without specifying what they want.
 */
function onLaunch(launchRequest, session, callback) {
    console.log("onLaunch requestId=" + launchRequest.requestId + ", sessionId=" + session.sessionId);
    getWelcomeResponse(callback);
}

/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session, callback) {
    console.log("onIntent requestId=" + intentRequest.requestId + ", sessionId=" + session.sessionId);
    var map = new HashMap();
    var intent = intentRequest.intent;
    var intentName = intentRequest.intent.name;
    if ("donate" === intentName) {
        //authenticate(intent, session, callback, map);
        testFunction(intent, session, callback, map);
    } else if ("give" === intentName) {
        authenticate(intent, session, callback, map);
    } else if ("AMAZON.HelpIntent" === intentName) {
        getWelcomeResponse(callback);
    } else if ("AMAZON.StopIntent" === intentName || "AMAZON.CancelIntent" === intentName) {
        handleSessionEndRequest(callback);
    } else {
        throw "Invalid intent";
    }  
}

/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId + ", sessionId=" + session.sessionId);
}

function handleSessionEndRequest(callback) {
    var cardTitle = "Session Ended";
    var speechOutput = "Thank you for donating. Have a nice day!";
    var shouldEndSession = true;
    callback({}, buildSpeechletResponse(cardTitle, speechOutput, null, shouldEndSession));
}

// --------------- Welcome and Confirmation Asks -----------------------

function getWelcomeResponse(callback) {
    var sessionAttributes = {};
    var cardTitle = "Welcome";
    var speechOutput = "Welcome to the Swift Donate App. " + "Please let Alexa know how much and to which organization you would like to donate!";
    var repromptText = "Please let us know the amount and organization which you would like to send your donation";
    var shouldEndSession = false;
    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}

function getConfirmation(intent, callback) {
    var sessionAttributes = {};
    var cardTitle = "Confirm";
    var speechOutput = "Please confirm that you want me to make a donation of " + intent.slots.Number.value + " dollars to " + intent.slots.Organization.value;
    var repromptText = "Please say the amount and the organization where you want to donate";
    var shouldEndSession = false;
    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}

function testFunction(intent, session, callback, map) {
    var accessToken = session.user.accessToken;
    if(accessToken == undefined) {
        var sessionAttributes = {};
        var cardTitle = "Authenticate Your Account";
        var speechOutput = "to start using this skill, please use the companion app to authenticate on Amazon";
        var repromptText = "please use the companion app on your device to authenticate on Amazon";
        var shouldEndSession = false;
        callback(sessionAttributes, linkAccountResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    } else {
        var options = {
            path: '/OffAmazonPayments/2013-01-01',
            query: {
                AccessToken: accessToken,              
                Action: 'CreateOrderReference',
                MWSAuthToken: 'amzn.mws.f2ab1187-f903-8cb4-9787-7625a5fd5af1',                
                Version: '2013-01-01'
            }
        }

        mws.request(options, function(e, result) {
            console.log(JSON.stringify(result));
        });
    }
}

// --------------- Operational Functions -----------------------

function authenticate(intent, session, callback, map) {
    var repromptText = null;
    var sessionAttributes = {};
    var shouldEndSession = false;
    var speechOutput = null;

    var url = "https://mws.amazonservices.com";   // PROD
    var path = "/OffAmazonPayments/2013-01-01"; //SANDBOX 
    var secret_key = 'wy9i/uMeFa9h8CSOZEPtWsH9000qINXT7ePjxGVB';//'2jyHYLVIhcoWwRqhQtHe7yaQ+6kd4NzlwNL+FTfM';

    var accessToken = session.user.accessToken;
    if(accessToken == undefined) {
        var sessionAttributes = {};
        var cardTitle = "Authenticate Your Account";
        var speechOutput = "to start using this skill, please use the companion app to authenticate on Amazon";
        var repromptText = "please use the companion app on your device to authenticate on Amazon";
        var shouldEndSession = false;
        callback(sessionAttributes, linkAccountResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    } else {
        //console.log(accessToken);
        createOrderReference(accessToken, url, path, secret_key, map);
    }
    
    //getConfirmation(intent, callback);

    if (intent.slots.Organization.value == null) {
        repromptText = "We could not find the organization.";
    }
}

function createOrderReference(accessToken, url, path, secret_key, map) {
    map.set("AWSAccessKeyId", encodeURIComponent('AKIAIMOK2JLCKB75X2QQ'));    
    map.set("Action", encodeURIComponent("CreateOrderReference"));    
    map.set("AccessToken", encodeURIComponent(accessToken));
    map.set("AssociateShippingAddress", "true");
    map.set("MWSAuthToken", encodeURIComponent("amzn.mws.c2ace77f-7a05-127a-8686-bd00dcc582ba"));
    map.set("SellerId", encodeURIComponent('A2F2AFRFUK32FS'));
    map.set("SignatureMethod", encodeURIComponent("HmacSHA256"));
    map.set("SignatureVersion", encodeURIComponent("2"));
    var date = new Date();
    map.set("Timestamp", encodeURIComponent(date.toISOString().split('.')[0]+"Z"));
    map.set("Version", encodeURIComponent("2013-01-01"));        

    return dispatchRequest(map, url, path, secret_key);
}

// ---------- Create Donate to do actual donation ----------------

function dispatchRequest(map, url, path, secret_key) {
    var formattedParameters = calculateStringToSignV2(map, url, path);
    console.log("This is formatted params: " + formattedParameters);
    var signature = sign(formattedParameters, secret_key);
    console.log("This is formatted signature: " + signature);    
    map.set("Signature", encodeURIComponent(signature));
    console.log(map);
    var response = postData(map, url, path, secret_key);
}

// --------------- Helper functions for the call ------------------------------------------

/* If Signature Version is 2, string to sign is based on following:
*
*    1. The HTTP Request Method followed by an ASCII newline (%0A)
*
*    2. The HTTP Host header in the form of lowercase host,
*       followed by an ASCII newline.
*
*    3. The URL encoded HTTP absolute path component of the URI
*       (up to but not including the query string parameters);
*       if this is empty use a forward '/'. This parameter is followed
*       by an ASCII newline.
*
*    4. The concatenation of all query string components (names and
*       values) as UTF-8 characters which are URL encoded as per RFC
*       3986 (hex characters MUST be uppercase), sorted using
*       lexicographic byte ordering. Parameter names are separated from
*       their values by the '=' character (ASCII character 61), even if
*       the value is empty. Pairs of parameter and values are separated
*       by the '&' character (ASCII code 38).
*
*/

function calculateStringToSignV2(map, url, path) {
    // Sort the parameters alphabetically by storing
    // in TreeMap structure
    // Set endpoint value
    
    //new URI(serviceUrl.toLowerCase());

    // Create flattened (String) representation

    /*
    var data = new StringBuilder();
    data.append("POST\n");
    data.append("mws.amazonservices.com");
    data.append("\n");
    data.append("/OffAmazonPayments_Sandbox/2013-01-01");
    data.append("\n");
    var sorted = getSortedString(map);
    data.append(sorted);
    console.log("This is sorted: " + sorted);
    */

    //return data.toString();
    var sorted = getSortedString(map);
    var stringToSign = "POST" + "\r\n" + "mws.amazonservices.com" + "\r\n" + "/OffAmazonPayments/2013-01-01" + "\r\n" + sorted; //_Sandbox

    return stringToSign.toString();
}

function getSortedString(map) {
    //var data = new StringBuilder();
    var stringArray = [];

    map.forEach(function(value, key) {
        //if(key == 'Timestamp') {
            //stringArray.push(key + "=" + urlEncode(value));
        //} else {
            stringArray.push(key + "=" + value);
        //}
    });

    var sortedStringArray = stringArray.sort();

    return sortedStringArray.join("&");
}

function timestamp() {
    var date = new Date();
    var y = date.getUTCFullYear().toString();
    var m = (date.getUTCMonth() + 1).toString();
    var d = date.getUTCDate().toString();
    var h = (date.getUTCHours() - 5).toString();
    var min = date.getUTCMinutes().toString();
    var s = date.getUTCSeconds().toString();

    if(m.length < 2) { m = "0" + m; }
    if(d.length < 2) { d = "0" + d; }
    if(h.length < 2) { h = "0" + h; }
    if(min.length < 2) { min = "0" + min; }
    if(s.length < 2) { s = "0" + s}

    var date = y + "-" + m + "-" + d;
    var time = h + ":" + min + ":" + s;
    return date + "T" + time + "Z";
}

function urlEncode(rawValue) {
    if(rawValue == null) {
        var value = "";
    } else {
        var value = rawValue;
    }
    
    var encoded = null;
    encoded = (encodeURIComponent(value)).replace("+", "%20").replace("*", "%2A").replace("%7E", "~");

    return encoded;
}

/*
 * Sign the text with the given secret key and convert to base64
 */

function sign(data, secret_key) {
    //var hmac256datahash = CryptoJS.HmacSHA256(data, secret_key);
    //var hmac256base64data = hmac256datahash.toString(CryptoJS.enc.Base64);

    var hmac256base64data = crypto.createHmac("SHA256", secret_key).update(data).digest('base64');
    console.log(hmac256base64data);
    return hmac256base64data;
}

// --------------- Post Function to the endpoint -----------------------

function postData(map, url, path, secret_key) {
    var swiftDonateOptions = {
        url: url + path,
        method: 'POST',
        //'content-language': 'en-US,en;q=0.5',
        //'content-length': getSortedString(map).length.valueOf(),
        //'Content-Type': 'application/x-www-form-urlencoded',
        form: {
            'AWSAccessKeyId' : map.get('AWSAccessKeyId'),
            'Action' : map.get('Action'),
            'AccessToken' : map.get('AccessToken'),
            'AssociateShippingAddress' : map.get('AssociateShippingAddress'),            
            'MWSAuthToken' : map.get('MWSAuthToken'),
            'SellerId' : map.get('SellerId'),
            'SignatureMethod' : map.get('SignatureMethod'),
            'SignatureVersion' : map.get('SignatureVersion'),
            'Timestamp' : map.get('Timestamp'),
            'Version' : map.get('Version'),
            'Signature' : map.get('Signature')            
        }
    };
    request(swiftDonateOptions, function(err, res, body) {
        if(err) {
            console.log(err);
        } else {
            console.log(res);
            console.log(body);
            if(body != '') {
                console.log(body);
                /*var payment = amazonPayments.connect({
                    environment: "/OffAmazonPayments_Sandbox/2013-01-01",
                    sellerId: map.get("SellerId"),
                    mwsAccessKey: 'AKIAIQ2ERYWRXMMUDFJA',
                    mwsSecretKey: '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',
                    clientId: 'amzn1.application-oa2-client.abe8cc6297f74a4bbbb7105890fa980a'
                });
                
                payment.api.getTokenInfo(map.get('AccessToken'), function(err, tokenInfo) {
                    console.log(tokenInfo);
                });*/
                
                speechOutput = "Thanks! This worked!";
                repromptText = "You are done for now";
                //callback(sessionAttributes, buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
            } else {
                console.error("Error: Transaction was not created");
                speechOutput = "Please try again, Recurring Transaction Failed";
                repromptText = "Please attempt donation again!";
                //callback(sessionAttributes, buildSpeechletResponse(intent.name, speechOutput, repromptText, shouldEndSession));
            }
        }
    });
}

// --------------- Helpers that build all of the responses -----------------------

function linkAccountResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "LinkAccount",
            title: "SessionSpeechlet - " + title,
            content: "SessionSpeechlet - " + output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "Simple",
            title: "SessionSpeechlet - " + title,
            content: "SessionSpeechlet - " + output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildResponse(sessionAttributes, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttributes,
        response: speechletResponse
    };
}